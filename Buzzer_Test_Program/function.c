#include "ftdi.h"

uint8_t match_started_f = 0;

bool get_device_list(void){
	// Create the device information list.
	ftStatus = FT_CreateDeviceInfoList(&numDevs);

	if (ftStatus == FT_OK) {
		printf("Number of devices is %d\n",numDevs);
	}
	else {
		printf("Failed to get FTDI device list.\n");
	}

	if (numDevs > 0) {
		
		// Allocate storage for list based on numDevs.
		devInfo = (FT_DEVICE_LIST_INFO_NODE*)malloc(sizeof(FT_DEVICE_LIST_INFO_NODE)*numDevs);
		
		// Get the device information list.
		ftStatus = FT_GetDeviceInfoList(devInfo,&numDevs);
		
		if (ftStatus == FT_OK) {
				printf("Got Devices\n");
			}
		else
			{
				printf("Failed to get device list.\n");
			}
			// Set flag if we got at least on device.
			return true;	
		} 
	return false;
}

bool write_data(uint8_t *data, DWORD data_size){
	DWORD bytes_written;
	uint8_t i;
	
	
	ftStatus = FT_Write(devInfo[connected_device_num].ftHandle, data, data_size, &bytes_written);
	if(ftStatus != FT_OK){
		printf("Write failed\n");

		return false;
	}
	else{
		//printf("Write successful\n");
		return true;
	}
	
}

bool read_data(uint8_t *data, DWORD data_size){
	DWORD bytes_received;
	
	FT_SetTimeouts(ftHandle,5000,0); 
	ftStatus = FT_Read(devInfo[connected_device_num].ftHandle, data, data_size, &bytes_received);
	if(ftStatus == FT_OK) {  
		if(bytes_received == data_size){   // FT_Read OK  
			//printf("Read successful\n");
		}  
		else{   // FT_Read Timeout  
			//printf("Read timeout\n");
			return false;
		} 
	} 
	else{  // FT_Read Failed 
		printf("Read failed\n");
		return false;
	} 
	return true;
}

bool close_device(void){
	FT_Close(devInfo[connected_device_num].ftHandle);

	if (ftStatus != FT_OK)
	{
		printf("Could not close FTDI device\n");
		return false;
	}
	else
	{
		return true;
	}
	return false;
}

FT_STATUS connect_device(uint8_t device_number){
	
	uint8_t i;
	
	printf("\n\nConnected FTDI:");
	for (i = 0; i < numDevs && i < 8; i++) {
		printf("\nDevice: %d:\n",i);
		printf(" 	Flags:         0x%x\n",devInfo[i].Flags);
		printf(" 	Type:          0x%x\n",devInfo[i].Type);
		printf(" 	ID:            0x%x\n",devInfo[i].ID);
		printf(" 	Local ID:      0x%x\n",devInfo[i].LocId);
		printf(" 	Serial Number: %s\n",devInfo[i].SerialNumber);
		printf(" 	Description:   %s\n",devInfo[i].Description);
	//	printf(" 	ftHandle =     0x%x\n",devInfo[i].ftHandle);
	}
	
	ftStatus = FT_Open(device_number, &devInfo[device_number].ftHandle);

		if (ftStatus != FT_OK)
		{
			printf("Could not open FTDI device #%i.\n", device_number);
		}
		else
		{
			connected_device_num = device_number;
		}
	return ftStatus;
}

uint8_t test(void){
	DWORD number_of_rx_bytes;
	DWORD number_of_tx_bytes;
	DWORD event_status;
	uint8_t rx_data[20];
	uint8_t tx_data[2];
	uint16_t msec;
	int i;

	tx_data[0] = cmd_test;
	
	printf("Test...\n");
	
	write_data(tx_data, 0x1); 			//Send 'test' command
	
	while(number_of_rx_bytes == 0){
		FT_GetStatus(devInfo[connected_device_num].ftHandle, \
					 &number_of_rx_bytes, \
					 &number_of_tx_bytes, \
					 &event_status); 
	}
	read_data(rx_data, number_of_rx_bytes);
		
	printf("%i\n", rx_data[0]);
	
	return 0x0;
}

void start_match(void){
	uint8_t tx_data[2];
	
	tx_data[0] = cmd_start_match;
	
	write_data(tx_data, 0x1);  			//Send 'start_match' command
	printf("Match Started\n");
	printf("Press button to Interrupt match\n");
	match_started_f = 1;	
	return;
}

void end_match(void){
	uint8_t tx_data[2];
	
	tx_data[0] = cmd_end_match;
	
	
	if(match_started_f == 1){
		printf("Match Ended\n");
		write_data(tx_data, 0x1);  			//Send 'end_match' command
		match_started_f = 0;
	}
	else{
		printf("Match Not Started");
	}	
}

void cancel(void){
	uint8_t tx_data[2];
	
	tx_data[0] = cmd_cancel;
	write_data(tx_data, 0x1);  			//Send 'cancel' command
	printf("Cancel\n");

}

uint8_t add_device(uint8_t handle_number){
	DWORD number_of_rx_bytes;
	DWORD number_of_tx_bytes;
	DWORD event_status;
	uint8_t tx_data[3];
	uint8_t rx_data;
	printf("Add device %d\n", handle_number);
	
	tx_data[0] = cmd_add_device;
	tx_data[1] = handle_number;

	write_data(tx_data, 0x2);  			//Send 'add_device' command and 'handle_number'
	while(number_of_rx_bytes == 0){
		FT_GetStatus(devInfo[connected_device_num].ftHandle, \
					 &number_of_rx_bytes, \
					 &number_of_tx_bytes, \
					 &event_status); 
	}
	read_data(&rx_data, 0x1);
	printf("%i\n",rx_data);
	return rx_data;
}

uint8_t remove_device(uint8_t handle_number){	
	DWORD number_of_rx_bytes;
	DWORD number_of_tx_bytes;
	DWORD event_status;
	uint8_t tx_data[3];
	uint8_t rx_data;
	printf("Remove device %d\n", handle_number);
		
	tx_data[0] = cmd_remove_device;
	tx_data[1] = handle_number;
	
	write_data(tx_data, 0x2);  			//Send 'remove_device' command and 'handle_number'
	while(number_of_rx_bytes == 0){
		FT_GetStatus(devInfo[connected_device_num].ftHandle, \
					 &number_of_rx_bytes, \
					 &number_of_tx_bytes, \
					 &event_status); 
	}
	read_data(&rx_data, 0x1);
	printf("%i\n",rx_data);
	return rx_data;
}

int request_connected_devices(void){
	DWORD number_of_rx_bytes;
	DWORD number_of_tx_bytes;
	DWORD event_status;
	uint8_t rx_data;
	uint8_t tx_data[2];
	
	tx_data[0] = cmd_list_connected_devices;	
	
	printf("List connected devices\n");
	write_data(tx_data,0x1);  			//Send 'list_connected_devices'
	while(number_of_rx_bytes == 0){
		FT_GetStatus(devInfo[connected_device_num].ftHandle, \
					 &number_of_rx_bytes, \
					 &number_of_tx_bytes, \
					 &event_status); 
	}
	read_data(&rx_data, 0x1);
	printf("0x%x\n",rx_data);
	return rx_data;
}

void request_device_status(uint8_t handle_number, uint8_t *rx_data){
	DWORD number_of_rx_bytes;
	DWORD number_of_tx_bytes;
	DWORD event_status;
	uint8_t tx_data[2];
	
	tx_data[0] = cmd_request_device_status;
	tx_data[1] = handle_number;
	
	write_data(tx_data,0x2);  			//Send 'cmd_request_serial_number'

}

int request_error_code(void){
	// EMPTY
}

void request_serial_number(uint8_t *serial_number){
	DWORD number_of_rx_bytes;
	DWORD number_of_tx_bytes;
	DWORD event_status;	
	uint8_t tx_data[2];
	
	tx_data[0] = cmd_request_serial_number;
	
	
	printf("Request serial number \n");
	write_data(tx_data, 1);
	while(number_of_rx_bytes == 0){
		FT_GetStatus(devInfo[connected_device_num].ftHandle, \
					 &number_of_rx_bytes, \
					 &number_of_tx_bytes, \
					 &event_status); 
	}
	read_data(serial_number, 0x4);	
}

void list_connected_devices(void){
	uint8_t list;
	list = request_connected_devices();

	if((list & 0x1) == 0x1){
		printf("RED_1 Connected\n");
	}
	if((list & 0x2) == 0x2){
		printf("RED_2 Connected\n");
	}
	if((list & 0x4) == 0x4){
		printf("RED_3 Connected\n");
	}
	if((list & 0x8) == 0x8){
		printf("YELLOW_1 Connected\n");
	}
	if((list & 0x10) == 0x10){
		printf("YELLOW_2 Connected\n");
	}
	if((list & 0x20) == 0x20){
		printf("YELLOW_3 Connected\n");
	}
	if((list & 0x3F) == 0x0){
		printf("No Devices Connected\n");
	}
}

void remove_device_from_list(uint8_t handle_number){
	DWORD number_of_rx_bytes;
	DWORD number_of_tx_bytes;
	DWORD event_status;
	uint8_t rx_data;
	uint8_t tx_data[2];
	
	tx_data[0] = cmd_remove_device_from_list;
	tx_data[1] = handle_number;
	
	write_data(tx_data, 0x2);
	
	while(number_of_rx_bytes == 0){
		FT_GetStatus(devInfo[connected_device_num].ftHandle, \
					 &number_of_rx_bytes, \
					 &number_of_tx_bytes, \
					 &event_status); 
	}
	read_data(&rx_data, 0x1);	
	printf("%i\n",rx_data);
}


void init_device(uint8_t *serial_number){
	DWORD number_of_rx_bytes;
	DWORD number_of_tx_bytes;
	DWORD event_status;	
	uint8_t tx_data[10];
	uint8_t rx_data;
	int i;
	
	tx_data[0] = 0xA5;
	tx_data[1] = 0x5A;
	tx_data[2] = 0x3E;
	for(i=0;i<4;i++){
		tx_data[i+3] = serial_number[i];
	}
	
	printf("Initializing... \n");
	write_data(tx_data, 7);
	printf("Complete\n");
	
	while(number_of_rx_bytes == 0){
		FT_GetStatus(devInfo[connected_device_num].ftHandle, \
					 &number_of_rx_bytes, \
					 &number_of_tx_bytes, \
					 &event_status); 
	}
	read_data(&rx_data, 0x1);	
	printf("%i\n",rx_data);	
}