#include "ftdi.h"
// Before running test,

uint8_t connected_device_num = 0;

// This section is for testing commands used in the interfacing API.
int main(){

	struct timeval current, previous;
    long seconds, useconds;    
	float mtime;
	
	DWORD number_of_rx_bytes = 0;
	DWORD number_of_tx_bytes = 0;
	DWORD event_status;	
	uint8_t tx_data;
	uint8_t tx_data_a[10];
	uint8_t rx_data[32];
	char c[30];
	char b[30] = {"add device"};
	int i,j;
	int command;
	uint8_t sn[4];
	int msec;
	get_device_list();
	
	gettimeofday(&previous, NULL);	
	
	if(numDevs < 1){
		printf("No devices Detected\n");
	}
	else{
		ftStatus = connect_device(0); 
		if(ftStatus != FT_OK) {  // FT_Open failed  
			printf("Failed to Open Device 0.\n");
			printf("Run the following: \n");
			printf("sudo rmmod ftdi_sio\n");
			printf("sudo rmmod usbserial\n");
			return -1; 
		} 
		else{
			printf("Opened Device 0.\n");
		}
	}
	
	
	
// ----------------- TEST -----------------------------		
	
	while(1){
		SKIP:
		printf("Enter command: ");
		scanf("%s",c);
		printf("\n");
	
	//*********  INITIALIZE *************			
		if(!strcmp(c,"init")){
			sn[0] = 0xD3;
			sn[1] = 0x52;
			sn[2] = 0x4E;
			sn[3] = 0xC8;
			init_device(sn);
			}
	
	//*********  EXIT PROGRAM *************			
		else if(!strcmp(c,"exit")){
			close_device();
			return 0;
		}
	
	//********* COM TEST *************		
		else if(!strcmp(c,"test")){
			i = test();
			printf("0x%x\n",i);
		}
		
	//********* ADD DEVICE *************		
		else if(!strcmp(c,"add")){
			printf("Device #: ");
			scanf("%s",c);
			printf(" \n");
			i = atoi(c);
			add_device(i);
		}
		
	//********* REMOVE DEVICE *************	
		else if(!strcmp(c,"remove")){
			printf("Device #: ");
			scanf("%s",c);
			printf(" \n");
			i = atoi(c);
			remove_device(i);
		}
	
	//***** REMOVE DEVICE FROM LIST *********	
		else if(!strcmp(c,"remdev")){
			printf("Device #: ");
			scanf("%s",c);
			printf(" \n");
			i = atoi(c);
			remove_device_from_list(i);
		}
		
	//********* REQUEST SERIAL *************	
		else if(!strcmp(c,"serial")){
			request_serial_number(rx_data);
			for(i=0;i<4;i++){
				printf("0x%x,",rx_data[i]);
			}
		}
		
	//********* CLEAR DEVICE LIST *************	
		else if(strcmp(c,"clear") == 0){
			for(i=0;i<6;i++){
				remove_device_from_list(i);
				}
			printf("List cleared \n");
		}
		
	//*********** END MATCH *************
		else if(strcmp(c,"end") == 0){
			end_match();
		}
	
	//************* CANCEL ***************
		else if(strcmp(c,"cancel") == 0){
			cancel();
		}	
	
	//********* PRINT DEVICE LIST *************
		else if(strcmp(c,"print") == 0){
			list_connected_devices();	
			printf(" \n");
		}
		
	//********* START MATCH *************	
		else if(strcmp(c,"start") == 0){
		
		number_of_rx_bytes = 0;	
		start_match();
		
		for(j=0;j<10;j++){		
			while(number_of_rx_bytes == 0){
					FT_GetStatus(devInfo[connected_device_num].ftHandle, \
							 &number_of_rx_bytes, \
							 &number_of_tx_bytes, \
							 &event_status); 
			}
			
			read_data(rx_data, number_of_rx_bytes);	
			
		// Each data packet from a buzzer is 7 bytes long. 
		// If multiple packets were received, divide total length by 7
		for(i=0;i<(number_of_rx_bytes / 7);i++){
			int offset = 7*i;
			msec = (rx_data[1+offset] << 7) | rx_data[0+offset];	// get milliseconds 
			printf("%02i:%02i:%02i.%03i\n", rx_data[4+offset], rx_data[3+offset], rx_data[2+offset], msec);
			
			switch(rx_data[5+offset]){	
					case RED_1:
					printf("Red 1 buzzed in!\n");
					break;
					case RED_2:
					printf("Red 2 buzzed in!\n");
					break;
					case RED_3:
					printf("Red 3 buzzed in!\n");
					break;
					case YELLOW_1:
					printf("Yellow 1 buzzed in!\n");
					break;
					case YELLOW_2:
					printf("Yellow 2 buzzed in!\n");
					break;
					case YELLOW_3:
					printf("Yellow 3 buzzed in!\n");	
					break;		
					case 0x6:
					printf("Interruption. \n");
					break;
				}
				printf("Status: %x\n",rx_data[7+offset]); 
				printf("Data Size: %i\n", number_of_rx_bytes);
				printf("\n");
			}
		}
	}
	
	//********* REQUEST STATUS *************
		else if(strcmp(c,"status") == 0){
			printf("Device #: ");
			scanf("%s",c);
			printf(" \n");
			i = atoi(c);
			
			request_device_status(i, rx_data);

			while(number_of_rx_bytes == 0){
					FT_GetStatus(devInfo[connected_device_num].ftHandle, \
							 &number_of_rx_bytes, \
							 &number_of_tx_bytes, \
							 &event_status); 
			}
			read_data(rx_data, number_of_rx_bytes);	
				
			switch(rx_data[0]){
				case RED_1:
				printf("Red 1 status: ");
				break;
				case RED_2:
				printf("Red 2 status: ");
				break;
				case RED_3:
				printf("Red 3 status: ");
				break;
				case YELLOW_1:
				printf("Yellow 1 status: ");
				break;
				case YELLOW_2:
				printf("Yellow 2 status: ");
				break;
				case YELLOW_3:
				printf("Yellow 3 status: ");	
				break;		
			}
			printf("%x\n",rx_data[1]);  
			//printf("%c",0x7); // Beep
			printf("\n");
		}
		
	//********* BAD COMMAND *************	
		else{
			printf("Bad command\n");
			
		}

	}
	return 0;
}
