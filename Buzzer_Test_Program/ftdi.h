#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include "ftd2xx.h"
//#include <WinTypes.h>
#include <sys/time.h>
#include <unistd.h>


#define cmd_init_device 0xA5


uint8_t send_command(uint8_t command,...);
void delay(void);
void dev_test(void);
bool get_device_list(void);
bool write_data(uint8_t *data, DWORD data_size);
bool read_data(uint8_t *data, DWORD data_size);
bool close_device(void);
FT_STATUS connect_device(uint8_t device_number);
uint8_t test(void);
void start_match(void);
void end_match(void);
uint8_t add_device(uint8_t handle_number);
uint8_t remove_device(uint8_t handle_number);
int request_connected_devices(void);
int request_error_code(void);
void request_serial_number(uint8_t *serial_number);
void request_device_status(uint8_t handle_number, uint8_t *rx_data);
void cancel(void);
void init_device(uint8_t *serial_number);
void reset_device(void);
void remove_device_from_list(uint8_t handle_number);
void quarter_delay(void);
void device_ready(void);
void list_connected_devices(void);
extern uint8_t ParsedRxBuffer[2048];
extern uint8_t RawRxBuffer[2048];



FT_HANDLE ftHandle;
FT_STATUS ftStatus;

//UCHAR LatencyTimer = 2;
DWORD EventDWord;
DWORD TxBytes;
DWORD BytesWritten;
DWORD RxBytes;
DWORD BytesReceived;
DWORD numDevs;
FT_DEVICE_LIST_INFO_NODE *devInfo;

extern uint8_t connected_device_num;
extern uint8_t match_started_f;

enum{
    cmd_test,
    cmd_start_match,
    cmd_end_match,
    cmd_add_device,     
    cmd_remove_device,
    cmd_list_connected_devices,
    cmd_request_device_status,
    cmd_request_error_code,
    cmd_request_serial_number,
	cmd_cancel,
	cmd_remove_device_from_list,
	cmd_reset
}usb_commands_t;

enum{
	RED_1,
	RED_2,
	RED_3,
	YELLOW_1,
	YELLOW_2,
	YELLOW_3,
	INTERRUPTION
}handle_name_t;
