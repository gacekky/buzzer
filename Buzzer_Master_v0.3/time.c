/*
 * File:   time.c
 * Author: gacek
 *
 * Created on September 12, 2018, 8:12 PM
 */

#include "time.h"
#include "device.h"

void get_time(struct time_t *time){
	time->msec = sys_time.msec;
	time->sec = sys_time.sec;
	time->min = sys_time.min;
    time->hour = sys_time.hour;
}

void timer_on(){
	T2CONbits.T2OUTPS = 0b1001;
	T2CONbits.T2CKPS = 1;

	PR2 = 200;
	TMR2 = 0;

	INTCONbits.GIE = 1;
	INTCONbits.PEIE = 1;

	PIR1bits.TMR2IF = 0;
	PIE1bits.TMR2IE = 1;
    sys_time.sec = 0;
    sys_time.msec = 0;
    sys_time.min = 0;
    sys_time.hour = 0;
    sys_timeout_f = 0;
	T2CONbits.TMR2ON = 1;
}

void timer_off(){
	PIE1bits.TMR2IE = 0;

	T2CONbits.TMR2ON = 0;

}