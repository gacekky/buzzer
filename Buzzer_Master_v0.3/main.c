#include "device.h"
#include "time.h"
//-------------------------------------------------------------------------
//------------------------------ INTERRUPT --------------------------------
//-------------------------------------------------------------------------
void __interrupt(high_priority) ISR(void){

    if(IOCBFbits.IOCBF5 == 1){
        ft221_read_data();              
		ft221_command_decode();
        usb_rx_fifo.data_ready_flag = 0;
        ft221_listen_mode();  
    }
    
    if(PIR1bits.TMR2IF == 1){
        sys_time.msec++;
        if(sys_time.msec == 1000){
            sys_time.sec++;
            sys_time.msec = 0;
        }
        if(sys_time.sec == 60){
            sys_time.min++;
            sys_time.sec = 0;
        }
        if(sys_time.min == 60){
            sys_time.hour++;
            sys_time.min = 0;
        }
        if(sys_time.hour == 24){
            sys_time.hour = 0;
        }
        
        PIR1bits.TMR2IF = 0;
    }    
    

}

//-------------------------------------------------------------------------
//--------------------------------- MAIN ----------------------------------
//-------------------------------------------------------------------------
int main(){    
    //------------ Device Initializations ---------------------------------
    device_init();              // Do device pin initializations
    spi_enable();               // Enable SPI for nRF24L01+ module
    nrf_enter_rx_mode();
    
    //TEST TEST TEST
//    ANSELCbits.ANSC7 = 0;
//    TRISCbits.TRISC7 = 0;
//    LATCbits.LATC7 = 0;
    
    
    //----------- nRF24L01+ Initializations -------------------------------
    nrf_init();                 // Initialize nRF24L01+ module
    fetch_handle_addresses();   // Make handle addresses from serial # in EEPROM
    fetch_device_serial(); 
    device_list.device_list_value = 0x0;
    fetch_device_list();        // Get device list from EEPROM
    //------------ FT221X Initializations ---------------------------------
    ft221_initialize();         // Initialize FT221X and put in listen mode
    ft221_listen_mode();
    while(1){
        _NOP_;
    }   
}



