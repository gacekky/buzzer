#include "types.h"

#ifndef TIME_H
#define	TIME_H 

struct time_t{
	uint16_t msec;
	uint8_t sec;
	uint8_t min;
    uint8_t hour;
};

void get_time(struct time_t *time);
void timer_on();
void timer_off();
struct time_t sys_time = {0,0,0,0};
uint8_t sys_timeout_f = 0;

#endif