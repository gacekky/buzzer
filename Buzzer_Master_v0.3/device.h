#include <xc.h>
#include "ft221xs.h"
#include "nrf24l01+.h"
#include "spi.h"
#include "nrf_config.h"
#include "types.h"

#ifndef _DEVICE_H
#define _DEVICE_H

//Device configuration bits
#pragma config FEXTOSC = OFF, RSTOSC = HFINT32, CLKOUTEN = OFF, CSWEN = OFF, FCMEN = ON
#pragma config MCLRE = OFF, PWRTE = OFF, WDTE = OFF, LPBOREN = OFF, BOREN = OFF, BORV = LOW, PPS1WAY = OFF, STVREN = OFF, DEBUG = OFF
#pragma config WRT = OFF, LVP = ON
#pragma config CP = 0, CPD = 1

#define TEST_OUT LATCbits.LATC7

#define NRF_CE          LATAbits.LATA5
#define NRF_CSN         LATAbits.LATA4
//#define LED0            LATCbits.LATC7
#define INTERRUPTION    PORTCbits.RC4
#define _NOP_           asm("NOP")

#define RED1    0x0
#define RED2    0x1
#define RED3    0x2
#define YELLOW1 0x3
#define YELLOW2 0x4
#define YELLOW3 0x5

#define dev_init 0xA5

int accumulator = 0;
int cancel_f = 0;
int end_match_f = 0;
uint8_t p1_f = 0;
uint8_t p2_f = 0;

typedef struct{
	unsigned	int red_1_address[5];
	unsigned	int red_2_address[5];
	unsigned	int red_3_address[5];
	unsigned	int yellow_1_address[5];
	unsigned	int yellow_2_address[5];
	unsigned	int yellow_3_address[5];
}handle_address_t;

typedef struct{
	unsigned    int RED_1 	 	:1;
	unsigned	int RED_2 	 	:1;
	unsigned	int RED_3 	 	:1;
	unsigned	int YELLOW_1 	:1;
	unsigned	int YELLOW_2 	:1;
	unsigned	int YELLOW_3 	:1;
}device_t;

typedef union{
	device_t device;
	int device_list_value;
}device_list_t;

device_list_t device_list;
handle_address_t handle_address;

enum{
    nop,
    set_device_address,
    remove_device_address,        
    send_error_code
}commands_t;

enum{
    test,
    start_match,
    end_match,
    add_device,     
    remove_device,
    list_connected_devices,
    request_device_status,
    request_error_code,
    request_serial_number,
	cancel,
    remove_device_from_list,
    reset
}usb_commands_t;

void    dev_init_command(void);
void    device_init(void);
void    mainloop(void);
void    test_command(void);
void    start_match_command(void);
void    add_device_command(void);
void    remove_device_command(void);
void    list_connected_devices_command(void);
void    request_device_status_command(void);
void    request_serial_number_command(void);
void    remove_device_from_list_command(void);
int     set_address_to_payload(int handle_number);
void    reset_command(void);

void    status_ok(void);
void    status_ng(void);

void    eeprom_unlock(void);
void    eeprom_write_data(int addr_h, int addr_l, int data_h, int data_l);
int     eeprom_read_data(int addr_h, int addr_l);
void    delay_us(int us);
void    delay_ms(int ms);

void    read_fifo(void);
void    set_addr_default(void);
void    fetch_device_address(void);
void    clear_address(void);
void    clear_TX_address(void);
void    clear_P0_address(void);
int     fetch_device_list(void);
void    fetch_serial_number(int *serial_number);
void    clear_device_list_bit(int handle_number);
void    set_device_list_bit(int handle_number);
void    fetch_handle_addresses(void);
int     pair_device_command(int handle_number);

void    ft221_command_decode(void);
void    fetch_device_address(void);
void    mainloop(void);
void    set_RX_address(int handle_number);

void    clear_device_list(void);

void    select_handle(int handle_number);
void    add_handle(int handle_number);
void    remove_handle(int handle_number);

void    fetch_master_serial_number(int *serial_number);
void    set_master_serial_number(int *sn);
void    remove_device_serial(int handle_number);
int     serial_number_check(void);
void    set_device_serial(int handle_number);
void    fetch_device_serial(void);

void end_match_command(void);
void cancel_command(void);
int dev_mask(int pipe_number);

#endif



