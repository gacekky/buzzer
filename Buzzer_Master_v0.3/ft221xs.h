#ifndef FT221XS_H
#define FT221XS_H

#define INPUT               0x1
#define OUTPUT              0x0
#define FT_CS               LATCbits.LATC0
#define FT_MIOSIO_TRIS      TRISCbits.TRISC2
#define FT_MIOSIO_LAT       LATCbits.LATC2
#define FT_MIOSIO_PORT      PORTCbits.RC2
#define FT_SCK_TRIS         TRISBbits.TRISB7
#define FT_SCK_LAT          LATBbits.LATB7
#define FT_MISO             PORTBbits.RB5

#define BANKSELECT          asm("BANKSEL LATC")
#define CLR_SCK             asm("BCF LATB,7")
#define SET_SCK             asm("BSF LATB,7")
#define CLR_MOSI            asm("BCF LATC,2") 
#define SET_MOSI            asm("BSF LATC,2") 

typedef struct{
    int data_ready_flag;
    int data_sent_flag;
    int fifo_data[32];
    int fifo_size;
}usb_fifo_t;

usb_fifo_t usb_tx_fifo;
usb_fifo_t usb_rx_fifo;

void ft221_write_data(void);
void ft221_read_data(void);
int  ft221_read_byte(void);
void ft221_write_byte(int data);
void send_zero(void);
void send_one(void);
void ft221_listen_mode(void);
void ft221_initialize(void);

#endif