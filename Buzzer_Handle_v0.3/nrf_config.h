#ifndef _NRF24_SETUP_H
#define _NRF24_SETUP_H

#define nrf_STATUS STATUS_u.value
#define nrf_STATUSbits STATUS_u.STATUS_bits

#define nrf_FIFO_STATUS FIFO_STATUS_u.value
#define nrf_FIFO_STATUSbits FIFO_STATUS_u.FIFO_STATUS_bits

typedef unsigned char uint8_t;

//-------------------- STATUS ----------------------------
typedef struct{
	uint8_t TX_FULL	:1;
	uint8_t RX_P_NO	:3;
	uint8_t MAX_RT		:1;
	uint8_t TX_DS		:1;
	uint8_t RX_DR		:1;
	uint8_t reserved	:1;
}STATUS_bits_t;

typedef union{
	STATUS_bits_t STATUS_bits;
	uint8_t value;
}STATUS_t;

//-------------------- FIFO STATUS ----------------------------
typedef struct{
	uint8_t RX_EMPTY	:1;
	uint8_t RX_FULL 	:1;
	uint8_t reserved1	:2;
	uint8_t TX_EMPTY	:1;
	uint8_t TX_FULL     :1;
	uint8_t TX_REUSE	:1;	
	uint8_t reserved2	:1;
}FIFO_STATUS_bits_t;

typedef union{
	FIFO_STATUS_bits_t FIFO_STATUS_bits;
	uint8_t value;
}FIFO_STATUS_t;

STATUS_t STATUS_u;

FIFO_STATUS_t FIFO_STATUS_u;


void nrf_update_STATUS(void);
void nrf_update_FIFO_STATUS(void);
void nrf_read_STATUS(void);
void nrf_read_FIFO_STATUS(void);
#endif