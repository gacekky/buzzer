#include <xc.h>
#include "nrf24l01+.h"
#include "nrf_config.h"
#include "spi.h"

#ifndef _DEVICE_H
#define _DEVICE_H

//Device configuration bits
#pragma config FEXTOSC = OFF, RSTOSC = HFINT1, CLKOUTEN = OFF, CSWEN = OFF, FCMEN = ON
#pragma config MCLRE = OFF, PWRTE = OFF, WDTE = OFF, LPBOREN = OFF, BOREN = OFF, BORV = LOW, PPS1WAY = OFF, STVREN = OFF, DEBUG = OFF
#pragma config WRT = OFF, LVP = ON
#pragma config CP = OFF, CPD = OFF

uint8_t remove_device_f = 0;

//#define RESET_SWITCH *******************ADD RESET SWITCH*****
//#define BUTTON  PORTCbits.RC1 **********ADD BUTTON***********
#define LATCH   LATCbits.LATC4
#define NRF_CE  LATAbits.LATA5
#define NRF_CSN LATAbits.LATA4
#define LED0 LATBbits.LATB7
#define _NOP_ asm("NOP")

typedef struct{
    uint8_t batt_level      :1;
    uint8_t reset_switch    :1;
    uint8_t error           :1;
    uint8_t reserved        :5;
}device_status_bits_t;

typedef union{
    device_status_bits_t bits;
    uint8_t value;
}device_status_t;

device_status_t device_status;

enum{
    nop,
    set_device_address,
    remove_device_address,        
    send_error_code
}commands_t;

typedef struct{
    uint8_t not_sure_yet     :1;
}error_bits_t;

typedef union{
    error_bits_t bits;
    uint8_t value;
}device_error_t;

device_error_t device_error;

typedef struct{
    uint8_t sn[4];
}serial_t;

serial_t device_serial;

void    command_decode(int cmd);
void    eeprom_unlock(void);
void    eeprom_write_data(int addr_h, int addr_l, int data);
int     eeprom_read_data(int addr_h, int addr_l);
void    delay_us(int us);
void    delay_ms(int ms);
void    read_fifo(void);
void    fetch_device_address(void);
void    clear_address(void);
void    fetch_device_status(void);
void    write_device_status(void);
void    device_init(void);
void    write_device_serial(void);
void    fetch_device_serial(void);

#endif


