#include "device.h"
void nrf_reg_test(void);

void nrf_init(void){
    NRF_CE = 0;                 //Set CE low before enabling nRF module
    NRF_CSN = 1;                //Set CSN high before enabling nRF module
    delay_ms(1);    //Last working = 1ms
    
    nrf_write_register(_CONFIG, 0x3A);
    nrf_write_register(_EN_AA, 0x1);
    nrf_write_register(_EN_RXADDR, 0x1);      
    nrf_write_register(_SETUP_AW, 0x3);  
    //nrf_write_register(_SETUP_RETR, 0x00);   
    nrf_write_register(_SETUP_RETR, 0xFF); 
    nrf_write_register(_RF_CH, 0x1);      
    nrf_write_register(_RF_SETUP, 0b00001110);
    nrf_write_register(_RX_PW_P0, 32);      
    nrf_write_register(_DYNPD, 0x1);     
    nrf_write_register(_FEATURE, 0b110);
   
    delay_ms(1);
}



void nrf_add_tx_fifo(int *data, int data_size){
    int i;
    for(i=0;i<data_size;i++){
        tx_fifo.payload[tx_fifo.payload_size] = data[i];
        tx_fifo.payload_size++;
    }
}

void nrf_send_tx_fifo(void){
    int i;
//---------------- WRITE PAYLOAD ---------------------    
    NRF_CSN = 0;                        //Chip Select
        spi_tx(_W_TX_PAYLOAD);           //Send TX payload
        for(i=0;i<tx_fifo.payload_size;i++){
            spi_tx(tx_fifo.payload[i]);
        }
    NRF_CSN = 1;

//-----------------SEND--------------------------------    
    NRF_CE = 1;                         // Pull CE high
    delay_us(10);                        // Delay 10uS 
    NRF_CE = 0;                         // Pull CE low

    delay_us(130);
    
    tx_fifo.payload_size = 0;
}

void nrf_read_packet(void){
    int i;
    
    NRF_CSN = 0;
        spi_tx(_R_RX_PL_WID);
        rx_fifo.payload_size = spi_tx(0xEE);
	NRF_CSN = 1;
    
    NRF_CSN = 0;
        spi_tx(_R_RX_PAYLOAD);
        for(i=0;i<rx_fifo.payload_size;i++){
        rx_fifo.payload[i] = spi_tx(0xEE);
        }
    NRF_CSN = 1;
    
    rx_fifo.pipe_number = nrf_STATUSbits.RX_P_NO;
    rx_fifo.fifo_read_flag = 1;    
}

int  nrf_read_register(int addr){
    int data;
    addr = (addr & 0x1F);               //Set read bits with - address & 0b00011111 = 000xxxxx

    NRF_CSN = 0;                        //Set chip select low
        spi_tx(addr);                   //Send address with status pointer
        data = spi_tx(0xEE);          //Get data via SPI_WR
    NRF_CSN = 1;                        //Release chip select

return data;                            //Return read data
}

void nrf_write_register(int addr, int data){
    addr = (addr | 0x20);               //Set write bit with - address | 0b00100000 = xx1xxxxx
    addr = (addr & 0x3F);               //Clear leading zeros with - address & 0b00111111 = 001xxxxx

    NRF_CSN = 0;                    //Set chip select low
        spi_tx(addr);         //Send address with status pointer
        spi_tx(data);			//Send data to write
    NRF_CSN = 1;                    //Release chip select

}

void nrf_update_STATUS(void){
	nrf_write_register(_STATUS, nrf_STATUS);
}

void nrf_read_STATUS(void){
	nrf_STATUS = nrf_read_register(_STATUS);
}

void nrf_read_FIFO_STATUS(void){
	nrf_FIFO_STATUS = nrf_read_register(_FIFO_STATUS);
}
