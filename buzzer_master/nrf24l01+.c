#include "device.h"
#include "time.h"
#include "types.h"


void nrf_reg_test(void);

void nrf_init(void){
    int i;

    for(i=0;i<32;i++){
        nrf_tx_fifo.payload[i] = 0;
    }
    nrf_tx_fifo.payload_size = 0;
    nrf_tx_fifo.pipe_number = 0;
    nrf_tx_fifo.fifo_read_flag = 0;
    nrf_tx_fifo.fifo_sent_flag = 0;

    for(i=0;i<32;i++){
        nrf_rx_fifo.payload[i] = 0;
    }
    nrf_rx_fifo.payload_size = 0;
    nrf_rx_fifo.pipe_number = 0;
    nrf_rx_fifo.fifo_read_flag = 0;
    nrf_rx_fifo.fifo_sent_flag = 0;
        
    NRF_CE = 0;                 //Set CE low before enabling nRF module
    NRF_CSN = 1;                //Set CSN high before enabling nRF module
    delay_ms(2000);

    nrf_write_register(_CONFIG, 0x7B);    
    nrf_write_register(_EN_AA, 0x3F);
    nrf_write_register(_EN_RXADDR, 0x3F);      
    nrf_write_register(_SETUP_AW, 0x3);  
    nrf_write_register(_SETUP_RETR, 0xFF);      
    nrf_write_register(_RF_CH, 0x1);      
    nrf_write_register(_RF_SETUP, 0b00001110);
    nrf_write_register(_RX_PW_P0, 32);  
    nrf_write_register(_RX_PW_P1, 32); 
    nrf_write_register(_RX_PW_P2, 32); 
    nrf_write_register(_RX_PW_P3, 32); 
    nrf_write_register(_RX_PW_P4, 32); 
    nrf_write_register(_RX_PW_P5, 32); 
    nrf_write_register(_DYNPD, 0x3F);     
    nrf_write_register(_FEATURE, 0b110);
     
    delay_ms(50);
    NRF_CE = 1;
}

void nrf_add_nrf_tx_fifo(int *data, int data_size){
    int i;
    for(i=0;i<data_size;i++){
        nrf_tx_fifo.payload[nrf_tx_fifo.payload_size + i] = data[i];
        nrf_tx_fifo.payload_size++;
    }
}

void nrf_write_nrf_tx_fifo(void){
    int i;
//---------------- WRITE PAYLOAD ---------------------    
    NRF_CSN = 0;                        //Chip Select
        spi_tx(_W_TX_PAYLOAD);           //Send TX payload
        for(i=0;i<nrf_tx_fifo.payload_size;i++){
            spi_tx(nrf_tx_fifo.payload[i]);
        }
    NRF_CSN = 1;

//-----------------SEND--------------------------------
    NRF_CE = 1;                         // Pull CE high
    delay_us(100);                        // Delay 10uS
    NRF_CE = 0;                         // Pull CE low
    
    nrf_tx_fifo.payload_size = 0;
    
}

void nrf_read_packet(int cmd, int handle_number){
    uint8_t i;
    uint8_t data[1] = {nop};   
       
    nrf_rx_fifo.pipe_number = nrf_STATUSbits.RX_P_NO;


    switch(cmd){
        case add_device:
            //set_address_to_payload(handle_number);
            break;
        case start_match:
            data[0] = nop;
            nrf_write_ack_payload(data, 1, nrf_rx_fifo.pipe_number); 
            break;
        case remove_device:
            data[0] = remove_device_address;
            nrf_write_ack_payload(data, 1, nrf_rx_fifo.pipe_number);
            break;
        case request_device_status:
            data[0] = nop;
            nrf_write_ack_payload(data, 1, nrf_rx_fifo.pipe_number); 
            break;
        case nop:
            data[0] = nop;
            nrf_write_ack_payload(data, 1, nrf_rx_fifo.pipe_number); 
            break; 
    }
//---- GET DATA -----------------
    NRF_CSN = 0;
        spi_tx(_R_RX_PL_WID);
        nrf_rx_fifo.payload_size = spi_tx(0xEE);        //read payload size, send junk
    NRF_CSN = 1;

    NRF_CSN = 0;
        spi_tx(_R_RX_PAYLOAD);
        for(i=0;i<nrf_rx_fifo.payload_size;i++){       
        nrf_rx_fifo.payload[i] = spi_tx(0xEE);
        }
    NRF_CSN = 1;

    if((cmd == start_match) && (nrf_rx_fifo.payload[0] == 0x0)){
        nrf_rx_fifo.status_byte = nrf_rx_fifo.payload[6];       // Get byte 6, status byte
        if(serial_number_check()){
            struct time_t time = {0,0,0,0};
            get_time(&time);
            usb_tx_fifo.fifo_data[0] = time.msec & 0x00FF;          // mSec low byte
            usb_tx_fifo.fifo_data[1] = (time.msec >> 8) & 0x00FF;   // mSec high byte            
            usb_tx_fifo.fifo_data[2] = time.sec;                    // Sec
            usb_tx_fifo.fifo_data[3] = time.min;                    // Min 
            usb_tx_fifo.fifo_data[4] = time.hour;
            usb_tx_fifo.fifo_data[5] = nrf_rx_fifo.pipe_number;     // Add which handle sent the status and
            usb_tx_fifo.fifo_data[6] = nrf_rx_fifo.status_byte;     // status to TX_FIFO, send to PC
            usb_tx_fifo.fifo_size = 7;     
            ft221_write_data();   
        } 
    }
   
}

int nrf_read_register(int addr){
    int data;
    addr = (addr & 0x1F);               //Set read bits with - address & 0b00011111 = 000xxxxx

    NRF_CSN = 0;                        //Set chip select low
        spi_tx(addr);             //Send address with status pointer
        data = spi_tx(0xEE);          //Get data via SPI_WR
    NRF_CSN = 1;                        //Release chip select

return data;                            //Return read data
}

void nrf_write_register(int addr, int data){
    addr = (addr | 0x20);               //Set write bit with - address | 0b00100000 = xx1xxxxx
    addr = (addr & 0x3F);               //Clear leading zeros with - address & 0b00111111 = 001xxxxx

    NRF_CSN = 0;                    //Set chip select low
        spi_tx(addr);         //Send address with status pointer
        spi_tx(data);			//Send data to write
    NRF_CSN = 1;                    //Release chip select

}

void nrf_enter_rx_mode(){  //DELETE
    NRF_CE = 1;
    delay_us(1300);
}

void nrf_exit_rx_mode(){    //DELETE
    NRF_CE = 0;
}

void nrf_write_ack_payload(int *data, int data_width, int handle){
	int i;
	NRF_CSN = 0;
		spi_tx(_W_ACK_PAYLOAD|handle);
		for(i=0;i<data_width;i++){
			spi_tx(data[i]);
		}
	NRF_CSN = 1;
}

