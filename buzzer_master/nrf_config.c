#include "device.h"

void nrf_update_STATUS(void){
	nrf_write_register(_STATUS, nrf_STATUS);
}

void nrf_read_STATUS(void){
	nrf_STATUS = nrf_read_register(_STATUS);
}
