#include "device.h"
#include "time.h"
#include "types.h"

// DEVICE INITIALIZATIONS
void device_init(void){
//-------------- Turn off analog pin functionality ----------------
    ANSELA = 0;
    ANSELB = 0;
    ANSELC = 0;

// ---------------------- Set PPS bits ----------------------------   
// PPS UNLOCK SEQUENCE
    INTCONbits.GIE = 0; 
    PPSLOCK = 0x55;
    PPSLOCK = 0xAA;
    PPSLOCKbits.PPSLOCKED = 0;
    
// SET SPI SCK AND MOSI PINS
    RC1PPS = 0b11001;
    RB6PPS = 0b11000;
    
// PPS LOCK SEQUENCE
    INTCONbits.GIE = 0;
    PPSLOCK = 0x55;
    PPSLOCK = 0xAA;
    PPSLOCKbits.PPSLOCKED = 0;
//----------------- Set Input and Output Pins -----------------------
    TRISCbits.TRISC1 = OUTPUT;   //NRF - MOSI
    TRISAbits.TRISA5 = OUTPUT;   //NRF - CE
    TRISAbits.TRISA4 = OUTPUT;   //NRF - CSN
    TRISAbits.TRISA2 = INPUT;   //NRF - IRQ
    TRISBbits.TRISB4 = INPUT;   //NRF - MISO
    TRISBbits.TRISB6 = OUTPUT;   //NRF - SCK 
    
    TRISBbits.TRISB7 = OUTPUT;   //FTDI - CLK
    TRISCbits.TRISC2 = INPUT;   //FTDI - MIOSIO
    TRISCbits.TRISC5 = INPUT;   //FTDI - CBUS3
    TRISCbits.TRISC0 = OUTPUT;   //FTDI - CS
    TRISBbits.TRISB5 = INPUT;   //FTDI - MISO
    TRISCbits.TRISC4 = INPUT;   //INTERRUPTION BUTTON
    
//--------------- Enable external interrupts ------------------------
    PIR0bits.INTF       = 0;       
    INTCONbits.GIE      = 1;      
    INTCONbits.PEIE     = 1;
    PIE0bits.INTE       = 1;
    INTCONbits.INTEDG   = 0;
}

// WAITS FOR AND EXECUTES COMMAND FROM PC
void ft221_command_decode(void){
    int command;
    
    nrf_write_register(_EN_RXADDR, 0x0);            // Turn off nrf reception

    command = usb_rx_fifo.fifo_data[0];         // Get command
    
    switch(command){
        case cancel:
            cancel_command();
            break;
            
        case end_match:
            end_match_command();
            break;   
        
        case test :                             // Test case, just blink LED
            test_command();
            break;

        case start_match :  
            start_match_command();
            break; 

        case add_device :
            add_device_command();
            break;

        case remove_device :
            remove_device_command();
            break;

        case list_connected_devices :                          
            list_connected_devices_command();
            break;

        case request_device_status :
            request_device_status_command();
            break;

        case request_error_code :
            break;

        case request_serial_number:
            request_serial_number_command();
            break;  

        case remove_device_from_list:
            remove_device_from_list_command();
            break;
            
        case dev_init:
            dev_init_command();
            break;
            
    }
    return;
}

void cancel_command(void){
    usb_tx_fifo.fifo_data[0] = 0;
    usb_tx_fifo.fifo_size = 1;
    ft221_write_data();
    cancel_f = 1;
    return;
}

void end_match_command(void){
    usb_tx_fifo.fifo_data[0] = 0;
    usb_tx_fifo.fifo_size = 1;
    ft221_write_data();
    end_match_f = 1;
    return;
}

void dev_init_command(void){
    int i;
    int sn[4];
    if(usb_rx_fifo.fifo_data[1] == 0x5A){
        if(usb_rx_fifo.fifo_data[2] == 0x3E){
            for(i=0;i<4;i++){
                sn[i] = usb_rx_fifo.fifo_data[i+3];
            }
            set_master_serial_number(sn);
            clear_device_list();
            
            for(i=0;i<6;i++){
                remove_device_serial(i);
            }
                
            usb_tx_fifo.fifo_data[0] = 1;
            usb_tx_fifo.fifo_size = 1;
            ft221_write_data();
            return;
        }
    }
    usb_tx_fifo.fifo_data[0] = 0;
    usb_tx_fifo.fifo_size = 1;
    ft221_write_data();
    return;
}

void test_command(void){
    usb_tx_fifo.fifo_data[0] = 0x2A;
    usb_tx_fifo.fifo_size = 1;
    ft221_write_data();
    return;
}

void start_match_command(void){
    int pipe_no;
    struct time_t time;
    cancel_f = 0;
    end_match_f = 0;
    ft221_listen_mode(); 						
    
    nrf_write_register(_EN_RXADDR, 0x3F);       // Enable nrf reception
    nrf_write_register(_EN_AA, 0x3F); 
    
   // sys_timeout_f == 0;
    timer_on();                                 // Start 5 min timer
    
MATCH_LOOP:    
    nrf_read_STATUS();
    while(nrf_STATUSbits.RX_P_NO == 0b111){ // && !sys_timeout_f){
        if(INTERRUPTION){                		// If interruption button was pressed
            get_time(&time);
            usb_tx_fifo.fifo_data[0] = time.msec & 0x00FF;          // mSec low byte
            usb_tx_fifo.fifo_data[1] = (time.msec >> 8) & 0x00FF;   // mSec high byte            
            usb_tx_fifo.fifo_data[2] = time.sec;                    // Sec
            usb_tx_fifo.fifo_data[3] = time.min;                    // Min 
            usb_tx_fifo.fifo_data[4] = time.hour;
            usb_tx_fifo.fifo_data[5] = 0x6;         // Send handle #6 
            usb_tx_fifo.fifo_data[6] = 0x0;			// Status 0
            usb_tx_fifo.fifo_size = 7;				
            ft221_write_data();
        }
        else if(end_match_f == 1){
            timer_off();
            end_match_f = 0;
            return;
        }
        else if(cancel_f == 1){
            timer_off();
            cancel_f = 0;
            return;
        }
        else{
            nrf_read_STATUS();
        }
    }
    
    nrf_read_packet(start_match,0);
    
    goto MATCH_LOOP;
    
    return;    
}

int dev_mask(int pipe_number){
    switch(pipe_number){
        case 0x0:
            return 0b00000001;
        case 0x1:
            return 0b00000010;
        case 0x2:
            return 0b00000100;
        case 0x3:
            return 0b00001000;
        case 0x4:
            return 0b00010000;
        case 0x5:
            return 0b00100000;            
    }
}

void add_device_command(void){
	int handle_number;
    cancel_f = 0;
    
	handle_number = usb_rx_fifo.fifo_data[1];			// Get handle number from USB fifo	
	
    set_addr_default();                                 // Set tx and rx address pipe zero to default
    nrf_write_register(_EN_RXADDR,0x01); 
    nrf_write_register(_EN_AA,0x01);  
    
    set_address_to_payload(handle_number);              // Places new handle address in payload of PIPE0    
    nrf_read_STATUS();
	while(!nrf_STATUSbits.RX_DR){				// While no handle button is pressed
        nrf_read_STATUS();
        if(cancel_f){
            cancel_f = 0;
            return;
        }
    }
    //---- Read first packet ----
    nrf_read_packet(add_device, handle_number);     // Get serial number from packet
    nrf_write_register(_STATUS,0b01110000);
    nrf_update_STATUS();         
    //---- Read second packet ---- 
    nrf_read_STATUS();
    while(!nrf_STATUSbits.RX_DR){
        nrf_read_STATUS();
    }
    nrf_read_packet(nop, handle_number);     // Get serial number from packet
    nrf_write_register(_STATUS,0b01110000);
    nrf_update_STATUS();         
    set_device_serial(handle_number);               // Put serial number into eeprom
    fetch_device_serial();                          // Update serial numbers
    
    set_RX_address(RED1);                   			// Reset PIPE0 to RED1 address
    
    nrf_write_register(_EN_RXADDR,0x3F);               // Enable nrf reception
    nrf_write_register(_EN_AA,0x3F);

    set_device_list_bit(handle_number);
    
    usb_tx_fifo.fifo_data[0] = 1;
    usb_tx_fifo.fifo_size = 1;
    ft221_write_data();

    return;
}

void remove_device_command(void){
	int handle_number;
    cancel_f = 0;
	
	handle_number = usb_rx_fifo.fifo_data[1];
    
    select_handle(handle_number);                   // Change TX address to handle number 
    nrf_write_register(_EN_RXADDR,handle_number);   // Only receive from device with handle number
    
    switch(handle_number){                          // Only receive from device with handle number
        case RED1:	
            nrf_write_register(_EN_RXADDR,0x01);   
            break;
            
        case RED2:	
            nrf_write_register(_EN_RXADDR,0x02); 
            break;

        case RED3:	
            nrf_write_register(_EN_RXADDR,0x04); 
            break;
        
        case YELLOW1:	
            nrf_write_register(_EN_RXADDR,0x08); 
            break;
            
        case YELLOW2:	
            nrf_write_register(_EN_RXADDR,0x10); 
            break;
            
        case YELLOW3:	
            nrf_write_register(_EN_RXADDR,0x20); 
            break;        

    }
    
    nrf_read_STATUS();
	while(!nrf_STATUSbits.RX_DR){				// While no handle button is pressed
        nrf_read_STATUS();
        if(cancel_f){
            cancel_f = 0;
            return;
        }
    }
    
    nrf_write_register(_EN_AA,0x3F);
    nrf_write_register(_EN_RXADDR,0x3F);
    
    //---- Read first packet ----
    nrf_read_packet(remove_device, handle_number);     // Get serial number from packet
    //---- Read second packet ----
    nrf_read_STATUS();
    while(!nrf_STATUSbits.RX_DR){
        nrf_read_STATUS();
    }
    nrf_read_packet(nop, handle_number);     // Get serial number from packet
    
    clear_device_list_bit(handle_number);
    remove_device_serial(handle_number);
    usb_tx_fifo.fifo_data[0] = 1;

    
    usb_tx_fifo.fifo_size = 1;
    ft221_write_data();

	return;
}

void list_connected_devices_command(void){
	fetch_device_list();
    usb_tx_fifo.fifo_data[0] = (device_list.device_list_value & 0x3F);
    usb_tx_fifo.fifo_size = 1;
    ft221_write_data();
    
    return;
}

void request_device_status_command(void){
	int handle_number;
	int command;			
    cancel_f = 0;
				
        handle_number = (usb_rx_fifo.fifo_data[1]); //Set RX address to handle number, (stored in usb_rx_fifo.fifo_data[1])
        switch(handle_number){                          // Only receive from device with handle number
            case RED1:	
                nrf_write_register(_EN_RXADDR,0x01);   
                break;

            case RED2:	
                nrf_write_register(_EN_RXADDR,0x02); 
                break;

            case RED3:	
                nrf_write_register(_EN_RXADDR,0x04); 
                break;

            case YELLOW1:	
                nrf_write_register(_EN_RXADDR,0x08); 
                break;

            case YELLOW2:	
                nrf_write_register(_EN_RXADDR,0x10); 
                break;

            case YELLOW3:	
                nrf_write_register(_EN_RXADDR,0x20); 
                break;        
        }

        nrf_read_STATUS();
        while(!nrf_STATUSbits.RX_DR){				// While no handle button is pressed
            nrf_read_STATUS();
            if(cancel_f){
                cancel_f = 0;
                return;
            }
        }
        nrf_read_packet(request_device_status,handle_number);
        
        usb_tx_fifo.fifo_data[0] = nrf_rx_fifo.pipe_number;     // Add which handle sent the status and
        usb_tx_fifo.fifo_data[1] = nrf_rx_fifo.status_byte;      // status to TX_FIFO, send to PC
        usb_tx_fifo.fifo_size = 2;                       
        ft221_write_data();
        
        return; 
	
}

void request_serial_number_command(void){
    int s_n[4];
    int i;
    
    fetch_master_serial_number(s_n);
    for(i=0;i<4;i++){
        usb_tx_fifo.fifo_data[i] = s_n[i];
    }
    usb_tx_fifo.fifo_size = 4;
    ft221_write_data();
	return;
}

void remove_device_from_list_command(void){
	int  handle_number;
    
    handle_number = usb_rx_fifo.fifo_data[1];
    fetch_device_list();
    clear_device_list_bit(handle_number); 	
    
    usb_tx_fifo.fifo_data[0] = 1;
    usb_tx_fifo.fifo_size = 1;
    ft221_write_data();
    
	return;
}

// ------------------------ DELAYS ------------------------------
void delay_ms(int ms){
    int i;
    for(i=0;i<=ms;i++){
        delay_us(0xEF);
    }
}

void delay_us(int us){
    int i;
    for(i=0;i<us;i++){
        _NOP_;
    }
}




//----------------- DEVICE PAIRING FUNCIONS ----------------------
// WRITE HANDLE ADDRESS TO ACK PAYLOAD
int set_address_to_payload(int handle_number){
	int payload[6] = {0};
	int i;
    int ret_val;

    nrf_write_register(_EN_RXADDR,0x1);
	payload[0] = set_device_address;
    
	switch(handle_number){
		case RED1 :
			for(i=0;i<5;i++){
				payload[i+1] = handle_address.red_1_address[i];
			}
			nrf_write_ack_payload(payload, 6, RED1);
			break;
            
		case RED2 :
			for(i=0;i<5;i++){
				payload[i+1] = handle_address.red_2_address[i];
			}
			nrf_write_ack_payload(payload, 6, RED1);
			break;
			
		case RED3 :
			for(i=0;i<5;i++){
				payload[i+1] = handle_address.red_3_address[i];
			}
			nrf_write_ack_payload(payload, 6, RED1);
			break;
		
		case YELLOW1 :
			for(i=0;i<5;i++){
				payload[i+1] = handle_address.yellow_1_address[i];
			}
			nrf_write_ack_payload(payload, 6, RED1);
			break;
		
		case YELLOW2 :
			for(i=0;i<5;i++){
				payload[i+1] = handle_address.yellow_2_address[i];
			}
			nrf_write_ack_payload(payload, 6, RED1);
			break;
		
		case YELLOW3 :
			for(i=0;i<5;i++){
				payload[i+1] = handle_address.yellow_3_address[i];
			}
			nrf_write_ack_payload(payload, 6, RED1);
			break;
	}    
    return ret_val;
}

// COMPILES HANDLE ADDRESSES FROM EEPROM USING SERIAL NUMBER
void fetch_handle_addresses(void){
	int serial_number[4];  //Gets serial number from EEPROM
	int i;
    
    fetch_master_serial_number(serial_number);
//---------------------------------------------------------------------		
	handle_address.red_1_address[0] = 0xA; 
	for(i=0;i<4;i++){
		handle_address.red_1_address[i+1] = serial_number[i];
	}
    set_RX_address(RED1);
//---------------------------------------------------------------------	
	handle_address.red_2_address[0] = 0xB; 
	for(i=0;i<4;i++){
		handle_address.red_2_address[i+1] = serial_number[i];
	}
    set_RX_address(RED2);
//---------------------------------------------------------------------		
	handle_address.red_3_address[0] = 0xC; 
	for(i=0;i<4;i++){
		handle_address.red_3_address[i+1] = serial_number[i];
	}
    set_RX_address(RED3);
//---------------------------------------------------------------------		
	handle_address.yellow_1_address[0] = 0xD; 
	for(i=0;i<4;i++){
		handle_address.yellow_1_address[i+1] = serial_number[i];
	}
    set_RX_address(YELLOW1);
//---------------------------------------------------------------------		
	handle_address.yellow_2_address[0] = 0xE; 
	for(i=0;i<4;i++){
		handle_address.yellow_2_address[i+1] = serial_number[i];
	}
    set_RX_address(YELLOW2);
//---------------------------------------------------------------------		
	handle_address.yellow_3_address[0] = 0xF; 
	for(i=0;i<4;i++){
		handle_address.yellow_3_address[i+1] = serial_number[i];
	}
    set_RX_address(YELLOW3);	
//---------------------------------------------------------------------	
}

// SETS PIPE RX ADDRESS TO PAIRED ADDRESS
void set_RX_address(int handle_number){
    int i;
    
    switch(handle_number){
        case RED1:	
            NRF_CSN = 0;
            spi_tx(_RX_ADDR_P0|0x20);
            for(i=0;i<5;i++){
                spi_tx(handle_address.red_1_address[i]);
            }
            NRF_CSN = 1;
            break;
            
        case RED2:	
            NRF_CSN = 0;
            spi_tx(_RX_ADDR_P1|0x20);
            for(i=0;i<5;i++){
                spi_tx(handle_address.red_2_address[i]);
            }
            NRF_CSN = 1;
            break;

        case RED3:	
            NRF_CSN = 0;
            spi_tx(_RX_ADDR_P2|0x20);
            spi_tx(handle_address.red_3_address[0]);
            NRF_CSN = 1;
            break;
        
        case YELLOW1:	
            NRF_CSN = 0;
            spi_tx(_RX_ADDR_P3|0x20);
            spi_tx(handle_address.yellow_1_address[0]);
            NRF_CSN = 1;
            break;
            
        case YELLOW2:	
            NRF_CSN = 0;
            spi_tx(_RX_ADDR_P4|0x20);
            spi_tx(handle_address.yellow_2_address[0]);
            NRF_CSN = 1;
            break;
            
        case YELLOW3:	
            NRF_CSN = 0;
            spi_tx(_RX_ADDR_P5|0x20);
            spi_tx(handle_address.yellow_3_address[0]);
            NRF_CSN = 1;
            break;           

    }
}

// REVERTS TX AND RX ADDRESS TO DEFAULT STORED IN EEPROM
void set_addr_default(void){
	int i;
	
	NRF_CSN = 0;
        spi_tx(_TX_ADDR|0x20);
        for(i=0;i<5;i++){
            spi_tx(0xD2);
        }
	NRF_CSN = 1;
    
    NRF_CSN = 0;
        spi_tx(_RX_ADDR_P0|0x20);
        for(i=0;i<5;i++){
            spi_tx(0xD2);
        }
	NRF_CSN = 1;
    
}


// -------------------- DEVICE REMOVAL FUNCTIONS -------------------------
// SETS TX ADDRESS TO ADDRESS OF PAIRED HANDLE
void select_handle(int handle_number){
    int i;
    
    switch(handle_number){
        case RED1:	
            NRF_CSN = 0;
            spi_tx(_TX_ADDR|0x20);
            for(i=0;i<5;i++){
                spi_tx(handle_address.red_1_address[i]);
            }
            NRF_CSN = 1;
            break;
            
        case RED2:	
            NRF_CSN = 0;
            spi_tx(_TX_ADDR|0x20);
            for(i=0;i<5;i++){
                spi_tx(handle_address.red_2_address[i]);
            }
            NRF_CSN = 1;
            break;

        case RED3:	
            NRF_CSN = 0;
            spi_tx(_TX_ADDR|0x20);
            for(i=0;i<5;i++){
                spi_tx(handle_address.red_3_address[i]);
            }
            NRF_CSN = 1;
            break;
        
        case YELLOW1:	
            NRF_CSN = 0;
            spi_tx(_TX_ADDR|0x20);
            for(i=0;i<5;i++){
                spi_tx(handle_address.yellow_1_address[i]);
            }
            NRF_CSN = 1;
            break;
            
        case YELLOW2:	
            NRF_CSN = 0;
            spi_tx(_TX_ADDR|0x20);
            for(i=0;i<5;i++){
                spi_tx(handle_address.yellow_2_address[i]);
            }
            NRF_CSN = 1;
            break;
            
        case YELLOW3:	
            NRF_CSN = 0;
            spi_tx(_TX_ADDR|0x20);
            for(i=0;i<5;i++){
                spi_tx(handle_address.yellow_3_address[i]);
            }
            NRF_CSN = 1;
            break;        

}
    
}


// --------------------- DEVICE LIST FUNCTIONS ------------------------
// CLEARS DEVICE LIST BYTE IN EERPOM
void clear_device_list(void){
    device_list.device_list_value = 0x0;
    eeprom_write_data(0x70,0x04,0,device_list.device_list_value);
}

// CLEARS CORRESPONDING BIT IN DEVICE LIST FOR PAIRED HANDLE
void clear_device_list_bit(int handle_number){
	switch(handle_number){
		case RED1 :
			device_list.device.RED_1 = 0;
			break;
		case RED2 :
			device_list.device.RED_2 = 0;
			break;
		case RED3 :
			device_list.device.RED_3 = 0;
			break;
		case YELLOW1 :
			device_list.device.YELLOW_1 = 0;
			break;
		case YELLOW2 :
			device_list.device.YELLOW_2 = 0;
			break;
		case YELLOW3 :
			device_list.device.YELLOW_3 = 0;
			break;			
	}	
	
	 eeprom_write_data(0x70,0x04,0,device_list.device_list_value);
}

// SETS CORRESPONDING BIT IN DEVICE LIST FOR PAIRED HANDLE
void set_device_list_bit(int handle_number){
	switch(handle_number){
		case RED1 :
			device_list.device.RED_1 = 1;
			break;
		case RED2 :
			device_list.device.RED_2 = 1;
			break;
		case RED3 :
			device_list.device.RED_3 = 1;
			break;
		case YELLOW1 :
			device_list.device.YELLOW_1 = 1;
			break;
		case YELLOW2 :
			device_list.device.YELLOW_2 = 1;
			break;
		case YELLOW3 :
			device_list.device.YELLOW_3 = 1;
			break;			
	}
    eeprom_write_data(0x70,0x04,0,device_list.device_list_value);	
}

// GETS DEVICE LIST FROM EEPROM, STORES IN STRUCT
int fetch_device_list(void){
	device_list.device_list_value = eeprom_read_data(0x70,0x04);     
}

// GETS HANDLE SERIAL NUMBER FROM EERPOM    
void fetch_device_serial(void){
    int i;
    
    for(i=0;i<4;i++){
        dev_serial.sn_red_1[i] = eeprom_read_data(0x70,(0x05 + i));     
    }
    for(i=0;i<4;i++){
        dev_serial.sn_red_2[i] = eeprom_read_data(0x70,(0x09 + i));     
    }
    for(i=0;i<4;i++){
        dev_serial.sn_red_3[i] = eeprom_read_data(0x70,(0x0D + i));     
    }
    for(i=0;i<4;i++){
        dev_serial.sn_yel_1[i] = eeprom_read_data(0x70,(0x11 + i));     
    }    
    for(i=0;i<4;i++){
        dev_serial.sn_yel_2[i] = eeprom_read_data(0x70,(0x15 + i));     
    }
    for(i=0;i<4;i++){
        dev_serial.sn_yel_3[i] = eeprom_read_data(0x70,(0x19 + i));     
    }        
}

// STORES HANDLE SERIAL NUMBER IN EEPROM
void set_device_serial(int handle_number){
    int i;
    switch(handle_number){
        case RED1:
            for(i=0;i<4;i++){
                eeprom_write_data(0x70,(0x05+i), 0x0, nrf_rx_fifo.payload[i+1]);
            }
            break;
        case RED2:
            for(i=0;i<4;i++){
                eeprom_write_data(0x70,(0x09+i), 0x0, nrf_rx_fifo.payload[i+1]);
            }
            break;
        case RED3:
            for(i=0;i<4;i++){
                eeprom_write_data(0x70,(0x0D+i), 0x0, nrf_rx_fifo.payload[i+1]);
            }
            break;            
        case YELLOW1:
            for(i=0;i<4;i++){
                eeprom_write_data(0x70,(0x11+i), 0x0, nrf_rx_fifo.payload[i+1]);
            }
            break;
        case YELLOW2:
            for(i=0;i<4;i++){
                eeprom_write_data(0x70,(0x15+i), 0x0, nrf_rx_fifo.payload[i+1]);
            }
            break;
        case YELLOW3:
            for(i=0;i<4;i++){
                eeprom_write_data(0x70,(0x19+i), 0x0, nrf_rx_fifo.payload[i+1]);
            }
            break;            
    }

}

// CHECKS FOR MATCHING SERIAL NUMBER
int serial_number_check(void){
    int i;
    
    switch(nrf_rx_fifo.pipe_number){
        case RED1:
            for(i=0;i<4;i++){
                if(nrf_rx_fifo.payload[i+1] != dev_serial.sn_red_1[i]){   //if current SN doesn't match stored SN, exit 0
                    return 0;
                }
            }
            break;
        case RED2:
            for(i=0;i<4;i++){
                if(nrf_rx_fifo.payload[i+1] != dev_serial.sn_red_2[i]){   //if current SN doesn't match stored SN, exit 0
                    return 0;
                }
            }    
            break;
        case RED3:
            for(i=0;i<4;i++){
                if(nrf_rx_fifo.payload[i+1] != dev_serial.sn_red_3[i]){   //if current SN doesn't match stored SN, exit 0
                    return 0;
                }
            }        
            break;
        case YELLOW1:
            for(i=0;i<4;i++){
                if(nrf_rx_fifo.payload[i+1] != dev_serial.sn_yel_1[i]){   //if current SN doesn't match stored SN, exit 0
                    return 0;
                }
            }     
            break;
        case YELLOW2:
            for(i=0;i<4;i++){
                if(nrf_rx_fifo.payload[i+1] != dev_serial.sn_yel_2[i]){   //if current SN doesn't match stored SN, exit 0
                    return 0;
                }
            }         
            break;
        case YELLOW3:
            for(i=0;i<4;i++){
                if(nrf_rx_fifo.payload[i+1] != dev_serial.sn_yel_3[i]){   //if current SN doesn't match stored SN, exit 0
                    return 0;
                }
            }     
            break;    
    }
    return 1;
}

// STORES HANDLE SERIAL NUMBER IN EEPROM
void remove_device_serial(int handle_number){
    int i;
    switch(handle_number){
        case RED1:
            for(i=0;i<4;i++){
                eeprom_write_data(0x70,(0x05+i), 0x0, 0x0);
            }
            break;
        case RED2:
            for(i=0;i<4;i++){
                eeprom_write_data(0x70,(0x09+i), 0x0, 0x0);
            }
            break;            
        case RED3:
            for(i=0;i<4;i++){
                eeprom_write_data(0x70,(0x0D+i), 0x0, 0x0);
            }
            break;
        case YELLOW1:
            for(i=0;i<4;i++){
                eeprom_write_data(0x70,(0x11+i), 0x0, 0x0);
            }
            break;
        case YELLOW2:
            for(i=0;i<4;i++){
                eeprom_write_data(0x70,(0x15+i), 0x0, 0x0);
            }
            break;
        case YELLOW3:
            for(i=0;i<4;i++){
                eeprom_write_data(0x70,(0x19+i), 0x0, 0x0);
            }
            break;            
    }

}



// --------------------- SETUP FUNCTIONS ------------------------------
// WRITES SERIAL NUMBER IN EEPROM
void set_master_serial_number(int *sn){
    int i;
    for(i=0;i<4;i++){    
        eeprom_write_data(0x70,i,0,sn[i]);
    }      
}

// GETS SERIAL NUMBER FROM EEPROM AS POINTER
void fetch_master_serial_number(int *serial_number){
    int i;
    
    for(i=0;i<4;i++){
        serial_number[i] = eeprom_read_data(0x70,i);
    }
} 


//------------------- EEPROM FUNCTIONS --------------------------------
// EEPROM UNLOCK SEQUENCE
void eeprom_unlock(void){
	INTCONbits.GIE = 0;
	NVMCON2 = 0x55;
	NVMCON2 = 0xAA;
	NVMCON1bits.WR = 1;
	//_NOP_;
	//_NOP_;
	INTCONbits.GIE = 1;
}
// WRITE DATA TO EEPROM
void eeprom_write_data(int addr_h, int addr_l, int data_h, int data_l){
	NVMCON1bits.NVMREGS = 1;
	NVMCON1bits.WREN = 1;
	
	NVMADRH = addr_h;
	NVMADRL = addr_l;
    NVMDATH = data_h;
	NVMDATL = data_l;
	eeprom_unlock();
	while(NVMCON1bits.WR == 1);
	
}
// READ DATA FROM EEMPROM
int eeprom_read_data(int addr_h, int addr_l){
	int data;
	
	NVMCON1bits.NVMREGS = 1;
	NVMADRH = addr_h;
	NVMADRL = addr_l;
	NVMCON1bits.RD = 1;
	
    data = (NVMDATH<<8) | NVMDATL ;
	
    return data;
}