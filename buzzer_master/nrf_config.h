#ifndef NRF_CONFIG_H
#define NRF_CONFIG_H

#define nrf_STATUS STATUS_u.value
#define nrf_STATUSbits STATUS_u.STATUS_bits

////-------------------- STATUS ----------------------------
typedef struct{
	unsigned int TX_FULL		:1;
	unsigned int RX_P_NO		:3;
	unsigned int MAX_RT		:1;
	unsigned int TX_DS		:1;
	unsigned int RX_DR		:1;
	unsigned int reserved	:1;
}STATUS_bits_t;
//
typedef union{
	STATUS_bits_t STATUS_bits;
	unsigned int value;
}STATUS_t;


STATUS_t STATUS_u;

void nrf_update_STATUS(void);

void nrf_read_STATUS(void);

#endif