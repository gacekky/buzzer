#ifndef _NRF24L01
#define _NRF24L01

// ----------------------- FIFO STRUCT -----------------------------
typedef struct{
    int payload[32];
    int payload_size;
    int pipe_number;
	int fifo_read_flag;
	int fifo_sent_flag;
    int status_byte;
}fifo_t;

typedef struct{
    int sn_red_1[4];
    int sn_red_2[4];
    int sn_red_3[4];
    int sn_yel_1[4];
    int sn_yel_2[4];
    int sn_yel_3[4];
}serial_t;

serial_t dev_serial;
fifo_t nrf_tx_fifo;
fifo_t nrf_rx_fifo;

//----------------------- REGISTERS --------------------------------
#define _CONFIG			0x00	//Configuration Register
#define _EN_AA			0x01	//Enable auto ACK
#define _EN_RXADDR		0x02	//Enable RX Addresses
#define _SETUP_AW		0x03	//Setup of address width
#define _SETUP_RETR		0x04	//Setup of automatic retransmission
#define _RF_CH   		0x05	//RF Channel
#define	_RF_SETUP		0x06	//RF Setup Register
#define _STATUS   		0x07	//Status Register
#define	_OBSERVE_TX		0x08	//Transmit observe Register
#define _RPD 			0x09	//Received Power Detector
#define _RX_ADDR_P0		0x0A	//Receive address data pipe 0
#define _RX_ADDR_P1		0x0B	//Receive address data pipe 1
#define _RX_ADDR_P2		0x0C	//Receive address data pipe 2
#define _RX_ADDR_P3		0x0D	//Receive address data pipe 3
#define _RX_ADDR_P4		0x0E	//Receive address data pipe 4
#define _RX_ADDR_P5		0x0F	//Receive address data pipe 5
#define _TX_ADDR		0x10
#define _RX_PW_P0		0x11
#define _RX_PW_P1		0x12
#define _RX_PW_P2		0x13
#define _RX_PW_P3		0x14
#define _RX_PW_P4		0x15
#define _RX_PW_P5		0x16
#define _FIFO_STATUS	0x17
#define _DYNPD			0x1C
#define _FEATURE		0x1D

//----------------------- COMMANDS --------------------------------
#define _R_RX_PAYLOAD           0b01100001
#define _W_TX_PAYLOAD           0b10100000
#define _FLUSH_TX               0b11100001
#define _FLUSH_RX				0b11100010
#define _REUSE_TX_PL			0b11100011			
#define _R_RX_PL_WID			0b01100000
#define _W_TX_PAYLOAD_NO_ACK    0b10110000
#define _R_RX_PAYLOAD			0b01100001
#define _W_ACK_PAYLOAD			0b10101000

//----------------------- nRF24L01+.c ------------------------------
void nrf_init(void);
void nrf_add_tx_fifo(int *data, int data_size);
void nrf_write_tx_fifo(void);
void nrf_read_packet(int cmd, int handle_number);
int  nrf_read_register(int addr);
void nrf_write_register(int addr, int data);
void nrf_write_ack_payload(int *data, int data_width, int handle);
void set_default_addr(void);
void nrf_enter_rx_mode();

#endif