#include "device.h"
#include "time.h"
#include "types.h"


#define FT221_MISO PORTBbits.RB5 

void ft221_interrupts_off();
void ft221_interrupts_on();

void ft221_write_data(){
	int i;
    ft221_interrupts_off();
    
	FT_CS = 0;
            ft221_write_byte(0x0);                              //Send WRITE command
            for(i=0;i<usb_tx_fifo.fifo_size;i++){
                ft221_write_byte(usb_tx_fifo.fifo_data[i]);     //Send data stored in TX FIFO
            }
        
	FT_CS = 1;
   
    usb_tx_fifo.data_sent_flag = 1;                         //Set DATA SENT flag
    
    FT_MIOSIO_TRIS = INPUT;                                 //Set MIOSIO tris back to input
    
    ft221_interrupts_on();
}

void ft221_read_data(){
	int i = 0;
    ft221_interrupts_off();
	
    FT_CS = 0;
		ft221_write_byte(2);                                //Send READ command
        while(!FT221_MISO){
			usb_rx_fifo.fifo_data[i] = ft221_read_byte();   //Read data while MISO == 1
            i++;
        }
        CLR_SCK;
	FT_CS = 1;
    
    usb_rx_fifo.data_ready_flag = 1;                        //Set DATA READY flag

    FT_MIOSIO_TRIS = INPUT;                                 //Set MIOSIO tris back to input
    
    ft221_interrupts_on();
}

int ft221_read_byte(void){
	int bit_buffer = 0;
    int byte_buffer  = 0;
	int i;
    FT_MIOSIO_TRIS = INPUT;
    
	for(i=0;i<8;i++){
		SET_SCK;
        delay_us(1);
		CLR_SCK;   
		bit_buffer = PORTCbits.RC2;
        delay_us(1);
		SET_SCK;
		byte_buffer = (byte_buffer << 1) | bit_buffer;
	}
	
    return byte_buffer;
}	

void ft221_write_byte(int data){
    int bitmask[8] = {0x80,0x40,0x20,0x10,0x8,0x4,0x2,0x1};
	int i;
    TRISCbits.TRISC2 = OUTPUT;
	for(i=0;i<8;i++){
        if((data & bitmask[i]) == bitmask[i]){    
			send_one();
		}
		else{
			send_zero();
		}
	}
}

void send_one(void){
    asm("BANKSEL LATB");    
	asm("BCF LATB,7");      //CLK
    asm("BANKSEL LATC");
	asm("BSF LATC,2");      //MIOSIO
    asm("BANKSEL LATB");
    asm("BSF LATB,7");      //CLK
	delay_us(1);
    asm("BANKSEL LATC");
    asm("BSF LATC,2");      //MIOSIO
	asm("BANKSEL LATB");
    asm("BCF LATB,7");      //CLK
    asm("BANKSEL LATC");
    asm("BSF LATC,2");      //MIOSIO
}

void send_zero(void){
    asm("BANKSEL LATB");
	asm("BCF LATB,7");      //CLK
    asm("BANKSEL LATC");
	asm("BCF LATC,2");
    asm("BANKSEL LATB");
	asm("BSF LATB,7");      //CLK
    delay_us(1);
    asm("BANKSEL LATC");
    asm("BCF LATC,2");
    asm("BANKSEL LATB");
	asm("BCF LATB,7");      //CLK
    asm("BANKSEL LATC");    
    asm("BCF LATC,2");          
}

void ft221_interrupts_on(){
    INTCONbits.PEIE = 1;
    
 //   IOCCFbits.IOCCF2 = 0;   //Clear MISO flag
    
    IOCBFbits.IOCBF5 = 0;
    IOCBNbits.IOCBN5 = 1;
    
    PIR0bits.IOCIF = 0;
    PIE0bits.IOCIE = 1;
}

void ft221_interrupts_off(){
    IOCBNbits.IOCBN5 = 0;
    PIE0bits.IOCIE = 0;
}


void ft221_listen_mode(void){    
    PIE0bits.IOCIE = 1;
    INTCONbits.PEIE = 1;
    
    TRISBbits.TRISB5 = 1;    
    IOCBNbits.IOCBN5 = 1;
    IOCBFbits.IOCBF5 = 0;
    
    PIR0bits.IOCIF = 0;
    INTCONbits.GIE = 1;
}

void ft221_initialize(void){
    int i;

    usb_tx_fifo.data_ready_flag = 0;
    usb_tx_fifo.data_sent_flag = 0;
    for(i=0;i<32;i++){
        usb_tx_fifo.fifo_data[i] = 0;
    }
    usb_tx_fifo.fifo_size = 0;
    
    usb_rx_fifo.data_ready_flag = 0;
    usb_rx_fifo.data_sent_flag = 0;
    for(i=0;i<32;i++){
        usb_rx_fifo.fifo_data[i] = 0;
    }
    usb_rx_fifo.fifo_size = 0;
    
    SSP2CON1bits.SSPEN = 0;
    PIE0bits.IOCIE = 1;
    INTCONbits.PEIE = 1;
    TRISBbits.TRISB5 = 1; 
    IOCBNbits.IOCBN5 = 1;
    IOCBFbits.IOCBF5 = 0;
    PIR0bits.IOCIF = 0;
    INTCONbits.GIE = 1;
    
    LATBbits.LATB7 = 0;     //Set SCK idle low
    FT_CS = 1;              //Set CHIP SELECT high
    
    nrf_rx_fifo.fifo_read_flag = 0;
}
