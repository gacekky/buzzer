#include "device.h"
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#define RESET_SIG PORTCbits.RC0 
#define CE_DELAY 500


void __interrupt(high_priority) ISR(void){
    nrf_read_STATUS();
   
    if(nrf_STATUSbits.RX_DR == 1){
        nrf_read_packet();
        nrf_STATUSbits.RX_DR = 1;
        nrf_STATUSbits.MAX_RT = 1;
        nrf_STATUSbits.TX_DS = 1;
        nrf_update_STATUS();
    }
    
    PIR0bits.INTF = 0;            //Clear INT0 Flag
}



int main() {
    int rst_sig_f = 0;
    int loop_count = 0; 
    int dly;
    
    device_init();              //Device initializations
    
    if(RESET_SIG == 1){
        rst_sig_f = 1;
    }
    spi_enable();               //Enable SPI
    nrf_init();                 //Initialize nRF24L01+ module
    fetch_device_address();     //Get device address from EEPROM
    write_device_serial();
    fetch_device_serial();
//------------Check if battery is low. 1 = OK, 0 = LOW--------------------
    if(PORTCbits.RC5 == 1){
        device_status.bits.batt_level = 1;
    }
    else{
        device_status.bits.batt_level = 0;
    }
    
//------------If the reset switch is ON, set address to default----------
    if(rst_sig_f == 1){
        clear_address();
        goto SKIP;
    }
    
    dly = eeprom_read_data(0x70,0x00);
    PIE0bits.INTE = 1;                                  // Turn on external interrupt
    PIR0bits.INTF = 0;                                  // Clear external interrupt flag   
//--------------- Send device status byte ------------------------------
TRANSMIT_DATA:   
    NRF_CSN = 0;                         
        spi_tx(_W_TX_PAYLOAD);           // Send TX payload
        spi_tx(0x00);                    // First packet identifier
        spi_tx(device_serial.sn[0]);     // Serial number byte 1
        spi_tx(device_serial.sn[1]);     // Serial number byte 2
        spi_tx(device_serial.sn[2]);     // Serial number byte 3   
        spi_tx(device_serial.sn[3]);     // Serial number byte 4
        spi_tx(device_status.value);     // Status
    NRF_CSN = 1; 
    
    NRF_CE = 1;                         // Pull CE high
        delay_us(CE_DELAY);                  // Last working = 500us
    NRF_CE = 0;                         // Pull CE low
    
    delay_us(1000);
    if(rx_fifo.fifo_read_flag){
        command_decode(rx_fifo.payload[0]);
    }

//----------------- Process delay --------------------------------    
    delay_us(10*dly);                      // Last working = 500us 
    loop_count = 0;
    rx_fifo.fifo_read_flag = 0;
//---------------- Send STATUS byte --------------------------------
SEND_JUNK:   
    NRF_CSN = 0;                         //Chip Select
        spi_tx(_W_TX_PAYLOAD);           //Send TX payload
        spi_tx(0x01);                    //Packet identifier
    NRF_CSN = 1;      
    
    NRF_CE = 1;                         // Pull CE high
        delay_us(CE_DELAY);                  // Last working = 500us
    NRF_CE = 0;                         // Pull CE low
 
    delay_us(1000);
    if(rx_fifo.fifo_read_flag){
        command_decode(rx_fifo.payload[0]);
    }


    if(remove_device_f == 1){
        clear_address();
    }
    
    SKIP:
    while(RESET_SIG);
    
    delay_ms(10);
            
    while(1){      
        LATCH = 0;
    }
        
}