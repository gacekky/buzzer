#include "device.h"

void spi_enable(void){
    SSPCON1bits.SSPEN = 0;       //Turn off MSSP
    SSPCON1bits.SSPM = 0x2;      //Configure for SPI master mode

    SSPSTATbits.SMP = 0;
    SSPCON1bits.CKP = 0;         //Clock idles low
    SSPSTATbits.CKE = 1;        //Send data on rising edge

    SSPCON1bits.SSPEN = 1;       //Turn on MSSP

    return;
}

int spi_tx(int data_tx){
    int data_rx;

    SSP1BUF = data_tx;           //Move byte of data into SSPBUF
    while(SSPSTATbits.BF == 0); //Loop while buffer not full

    data_rx = SSP1BUF;             //Do dummy read to clear SSPBUF
    while(SSP1STATbits.BF == 1); //Loop while buffer full

    return data_rx;
}