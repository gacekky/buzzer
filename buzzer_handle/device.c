#include "device.h"

void device_init(void){
//************** Turn off analog pin functionality ***************
    ANSELA = 0;
    ANSELB = 0;
    ANSELC = 0;
    
//****************** Turn Controller ON **************************
    TRISCbits.TRISC4 = 0;   //LATCH 
    LATCH = 1;              //Set latch bit high, latch controller ON

// ****************** Set PPS bits *******************************    
// PPS UNLOCK SEQUENCE
    INTCONbits.GIE = 0; 
    PPSLOCK = 0x55;
    PPSLOCK = 0xAA;
    PPSLOCKbits.PPSLOCKED = 0;

    RC1PPS = 0b11001;
    RB6PPS = 0b11000;
  
// PPS LOCK SEQUENCE
    INTCONbits.GIE = 0;
    PPSLOCK = 0x55;
    PPSLOCK = 0xAA;
    PPSLOCKbits.PPSLOCKED = 0;
    

//*************** Set input and output bits **********************
    TRISCbits.TRISC1 = 0;   //MOSI
    TRISAbits.TRISA5 = 0;   //CE
    TRISAbits.TRISA4 = 0;   //CSN
    TRISAbits.TRISA2 = 1;   //IRQ
    TRISBbits.TRISB4 = 1;   //MISO
    TRISBbits.TRISB6 = 0;   //SCK 
    TRISCbits.TRISC5 = 1;   //BATT_MON
    TRISCbits.TRISC4 = 0;   //LATCH 
    TRISBbits.TRISB7 = 0;   //LED0 
    TRISCbits.TRISC0 = 1;   //RESET_SIG
    TRISCbits.TRISC2 = 0;   //ON_SIG
    
//************* Enable external interrupts ***********************
    PIR0bits.INTF       = 0;       
    INTCONbits.GIE      = 1;      
    INTCONbits.PEIE     = 1;
    PIE0bits.INTE       = 1;
    INTCONbits.INTEDG   = 0;
    NRF_CE = 1;
    rx_fifo.fifo_read_flag = 0;
}

void delay_ms(int ms){
    int i;
    for(i=0;i<=ms;i++){
        delay_us(0xEF);
    }
}

void delay_us(int us){
    int i;
    for(i=0;i<us;i++);
}

void eeprom_unlock(void){
	INTCONbits.GIE = 0;
	NVMCON2 = 0x55;
	NVMCON2 = 0xAA;
	NVMCON1bits.WR = 1;
	_NOP_;
	_NOP_;
	INTCONbits.GIE = 1;
}

void eeprom_write_data(int addr_h, int addr_l, int data){
	NVMCON1bits.NVMREGS = 1;
	NVMCON1bits.WREN = 1;
	
	NVMADRH = addr_h;
	NVMADRL = addr_l;
	NVMDAT = data;
	eeprom_unlock();
	while(NVMCON1bits.WR == 1);
	
}

int eeprom_read_data(int addr_h, int addr_l){
	int data;
	
	NVMCON1bits.NVMREGS = 1;
	NVMADRH = addr_h;
	NVMADRL = addr_l;
	NVMCON1bits.RD = 1;
	
    data = (NVMDATH<<8) | NVMDATL ;
	
    return data;
}

void command_decode(int cmd){
    int i;
    i = cmd;

    switch (cmd){
        case nop:
            break;
            
        case set_device_address:
            for(i=0;i<5;i++){
                eeprom_write_data(0x70,i,rx_fifo.payload[i+1]);
            }
            break;
    
        case remove_device_address:
            remove_device_f = 1;
            break;
                 
        case send_error_code:
            //fix -- send error code
            break;
        default:
            device_status.bits.error = 1;
    }
}

void fetch_device_address(void){
    int i;
    int tx_address[5];
    
    for(i=0;i<5;i++){    
        tx_address[i] = eeprom_read_data(0x70,i);
    }
    
    NRF_CSN = 0;            
        spi_tx(_TX_ADDR | 0x20);       
        for(i=0;i<5;i++){
            spi_tx(tx_address[i]);
        }
    NRF_CSN = 1;
    
    NRF_CSN = 0;            
        spi_tx(_RX_ADDR_P0 | 0x20);       
        for(i=0;i<5;i++){
            spi_tx(tx_address[i]);
        }
    NRF_CSN = 1;
    
}

void clear_address(void){
    int i;
    for(i=0;i<5;i++){
        eeprom_write_data(0x70,i,0xD2);
    }
    fetch_device_address();
}

void fetch_device_status(void){ 
    device_status.value = eeprom_read_data(0x70,0x05);
}

void write_device_status(void){
    eeprom_write_data(0x70,0x05,device_status.value);
}

void fetch_device_serial(void){
    int i;
    
    for(i=0;i<4;i++){
        device_serial.sn[i] = eeprom_read_data(0x70,(0x06+i));
    }
}

void write_device_serial(void){
    eeprom_write_data(0x70,0x06,0x01);
    eeprom_write_data(0x70,0x07,0x01);
    eeprom_write_data(0x70,0x08,0x01);
    eeprom_write_data(0x70,0x09,0x07);
}